"""Thread_test consists of TestThread class and nessesary exceptions."""

import threading
import time


class MainFunctionDoesNotExist(Exception):
    """Main function is None."""

    pass


class PreStartExecutionError(Exception):
    """PreStart function failed to execute."""

    pass


class PreExitExecutionError(Exception):
    """PreExit function failed to execute."""

    pass


class MainExecutionError(Exception):
    """Main function failed to execute."""

    pass


class PreStartAttributeError(Exception):
    """PreStart function invalid args."""

    pass


class PreExitAttributeError(Exception):
    """PreExit function invalid args."""

    pass


class MainAttributeError(Exception):
    """Main function invalid args."""

    pass


class TestThread(threading.Thread):
    """Main thread class.

    Args:
        DEFAULT_THREAD_ID (int): Contains default id of thread.
        Default: 0
        DEFAULT_THREAD_NAME (str): Contains default name of thread.
        Default: "MainThread"
        DEFAULT_ARGS (dict): Contains default arguments for functions.
        Default: empty
        DEFAULT_DELAY (int): Contains default arguments for functions.
        Default: 1
        DEFAULT_LOOP_FACTOR (int): Contains default arguments for functions.
        Default: 1
    """

    DEFAULT_THREAD_ID = 0
    DEFAULT_THREAD_NAME = "MainThread"
    DEFAULT_ARGS = {}
    DEFAULT_DELAY = 0
    DEFAULT_LOOP_FACTOR = 1

    def __init__(self, **kwargs):
        """Init func.

        Kwargs:
            main(func): function of thread.

            thread_id (int, optional): id of thred.
            Default to 0.

            thread_name(str, optional): name of thread.
            Default to "MainThread"

            delay(int, optional): thread delay in cycle.
            Default to 1

            loop_factor(int, optional): Max amount of loops.
            loop_factor = -1 - exit from cycle
            loop_factor = 0 - cycle for all of eternity
            loop_factor = 1...n - cycle until loop_count >=loop_factor
            Default to 1

            pre_start(func, optional): function to execute before main.
            Default to None

            pre_exit(func, optional): function to execute before exit.
            Default to None

            pre_start_args(dict, optional): arguments for pre_start function.
            Default to empty

            pre_exit_args(dict, optional): arguments for pre_exit function.
            Default to empty

            main_function_args(dict, optional): arguments for main function.
            Default to empty

        """
        threading.Thread.__init__(self)
        self.thread_id = kwargs.get('thread_id',
                                    self.DEFAULT_THREAD_ID)
        self.thread_name = kwargs.get('thread_name',
                                      self.DEFAULT_THREAD_NAME)
        self.delay = kwargs.get('delay',
                                self.DEFAULT_DELAY)
        self.loop_factor = kwargs.get('loop_factor',
                                      self.DEFAULT_LOOP_FACTOR)
        self._pre_start = kwargs.get('pre_start',
                                     None)
        self._pre_exit = kwargs.get('pre_exit',
                                    None)

        self._main_function = kwargs.get('main', None)
        if self._main_function is None:
            raise MainFunctionDoesNotExist("Main function is not passed.")

        self._pre_start_args = kwargs.get('pre_start_args',
                                          self.DEFAULT_ARGS)
        self._pre_exit_args = kwargs.get('pre_exit_args',
                                         self.DEFAULT_ARGS)
        self._main_function_args = kwargs.get('main_function_args',
                                              self.DEFAULT_ARGS)
        self.loop_count = 0

    def run(self):
        """Run thread."""
        if self._pre_start is not None:
            try:
                self._pre_start(**self._pre_start_args)
            except (AttributeError, TypeError) as e:
                raise PreStartAttributeError(str(e))
            except Exception as e:
                raise PreStartExecutionError(str(e))
        while (self.loop_count < self.loop_factor and
               self.loop_factor != -1) or self.loop_factor == 0:
            try:
                self._main_function(**self._main_function_args)
            except (AttributeError, TypeError) as e:
                raise MainAttributeError(str(e))
            except Exception as e:
                raise MainExecutionError(str(e))
            if (self.loop_factor > 1 or self.loop_factor != 0):
                time.sleep(self.delay)
            self.loop_count += 1

        if self._pre_exit is not None:
            try:
                self._pre_exit(**self._pre_exit_args)
            except (AttributeError, TypeError) as e:
                raise PreExitAttributeError(str(e))
            except Exception as e:
                raise PreExitExecutionError(str(e))
