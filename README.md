# test_task_Alnicko

## Task description.

Write some Python3 threading class:
It should call one of the following functions:
 pre-start - called before start (optional)
 main - main threading function
pre-exit - called before exit (optional)
Any number of parameters can be passed in function any function
main threading function can be call many time with some delay
Should be able to stop thread

What I expect to get at the beginning:

* Tasks list.
* Estimated time for all tasks.
* Block diagram of the program.
* Threading class code
* Tests
* Code should be in git

## Tests

```
$ coverage run --include 'thread_test.py' tests.py -v
test_main_only_with_args (__main__.ThreadTestCase)
Main function only with args. ... ok
test_main_only_without_args (__main__.ThreadTestCase)
Main function only. ... ok
test_main_with_exception_in_function (__main__.ThreadTestCase)
Main function error. ... ok
test_main_with_incorrect_args (__main__.ThreadTestCase)
Main function with incorrect args. ... ok
test_multiple_loop_factor (__main__.ThreadTestCase)
Multiple loop factor. ... ok
test_multiple_seconds_delay (__main__.ThreadTestCase)
Longer delay test. ... ok
test_pre_exit_with_args (__main__.ThreadTestCase)
Pre_exit function with args. ... ok
test_pre_exit_with_exception_in_function (__main__.ThreadTestCase)
Pre_exit function error. ... ok
test_pre_exit_with_incorrect_args (__main__.ThreadTestCase)
Pre_exit function with incorrect args. ... ok
test_pre_exit_without_args (__main__.ThreadTestCase)
Pre_exit function without args. ... ok
test_pre_start_with_args (__main__.ThreadTestCase)
Pre_start function with args. ... ok
test_pre_start_with_exception_in_function (__main__.ThreadTestCase)
Pre_start function error. ... ok
test_pre_start_with_incorrect_args (__main__.ThreadTestCase)
Pre_start function with incorrect args. ... ok
test_pre_start_without_args (__main__.ThreadTestCase)
Pre_start function without args. ... ok
test_without_main (__main__.ThreadTestCase)
No main function. ... ok

----------------------------------------------------------------------
Ran 15 tests in 2.006s

OK

$ coverage report -m
Name             Stmts   Miss  Cover   Missing
----------------------------------------------
thread_test.py      62      0   100%

```

## Tasks list.

1. Investigation.
2. Create diagrams for TestThread class to clarify the workflow.
3. Design TestThread class.
4. Write unittest for TestThread class.
5. Write TestThread class.  
6. Loop functionality and delays for main threading function

## Estimated time for all tasks.

1. Investigation - 20 min.
2. Create diagrams for TestThread class to clarify the workflow - 20 min.
3. Design TestThread class - 15 min.
4. Write unittest for TestThread class - 1,5 hours.
5. Write TestThread class - 35 min.
6. Loop functionality and delays for main threading function - 1 hour

**Total:** 4h

## Notes

link: https://docs.python.org/3.6/library/threading.html

Class based on threading.Thread class.
link: https://docs.python.org/3.6/library/threading.html#threading.Thread
