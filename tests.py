"""Unittests for thread_test."""

import unittest

import thread_test


class ThreadTestCase(unittest.TestCase):
    """TestCase for Country db model."""

    def setUp(self):  # noqa
        """Setup parameters for test-cases."""
        self.thread_class = thread_test.TestThread

        self.params = {
            "first": "1",
            "second": "2",
            "third": "3"
        }
        self.incorrect_params = {
            "first": "1",
            "second": "2"
        }

        def function(first, second, third):
            """Test function."""
            pass

        def function_without_args():
            """Test function."""
            pass

        def incorrect_function(first, second, third):
            """Test incorrect function."""
            raise RuntimeError()

        self.test_function = function
        self.test_incorrect_function = incorrect_function
        self.test_function_without_args = function_without_args

    def tearDown(self):
        """Remove parameters for test-cases."""
        self.thread_class = None

    def test_main_only_without_args(self):
        """Main function only."""
        thread = self.thread_class(main=self.test_function_without_args)
        thread.start()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_main_only_with_args(self):
        """Main function only with args."""
        thread = self.thread_class(main=self.test_function,
                                   main_function_args=self.params)
        thread.start()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_without_main(self):
        """No main function."""
        with self.assertRaises(thread_test.MainFunctionDoesNotExist):
            thread = self.thread_class()
            thread.run()

    def test_main_with_incorrect_args(self):
        """Main function with incorrect args."""
        with self.assertRaises(thread_test.MainAttributeError):
            thread = self.thread_class(
                main=self.test_function,
                main_function_args=self.incorrect_params)
            thread.run()

    def test_main_with_exception_in_function(self):
        """Main function error."""
        with self.assertRaises(thread_test.MainExecutionError):
            thread = self.thread_class(main=self.test_incorrect_function,
                                       main_function_args=self.params)
            thread.run()

    def test_pre_start_without_args(self):
        """Pre_start function without args."""
        thread = self.thread_class(main=self.test_function_without_args,
                                   pre_start=self.test_function_without_args)
        thread.start()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_pre_start_with_args(self):
        """Pre_start function with args."""
        thread = self.thread_class(main=self.test_function_without_args,
                                   pre_start=self.test_function,
                                   pre_start_args=self.params)
        thread.start()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_pre_start_with_incorrect_args(self):
        """Pre_start function with incorrect args."""
        with self.assertRaises(thread_test.PreStartAttributeError):
            thread = self.thread_class(
                main=self.test_function_without_args,
                pre_start=self.test_function_without_args,
                pre_start_args=self.incorrect_params)
            thread.run()

    def test_pre_start_with_exception_in_function(self):
        """Pre_start function error."""
        with self.assertRaises(thread_test.PreStartExecutionError):
            thread = self.thread_class(main=self.test_function_without_args,
                                       pre_start=self.test_incorrect_function,
                                       pre_start_args=self.params)
            thread.run()

    def test_pre_exit_without_args(self):
        """Pre_exit function without args."""
        thread = self.thread_class(main=self.test_function_without_args,
                                   pre_exit=self.test_function_without_args)
        thread.start()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_pre_exit_with_args(self):
        """Pre_exit function with args."""
        thread = self.thread_class(main=self.test_function_without_args,
                                   pre_exit=self.test_function,
                                   pre_exit_args=self.params)
        thread.start()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_pre_exit_with_incorrect_args(self):
        """Pre_exit function with incorrect args."""
        with self.assertRaises(thread_test.PreExitAttributeError):
            thread = self.thread_class(main=self.test_function_without_args,
                                       pre_exit=self.test_function,
                                       pre_exit_args=self.incorrect_params)
            thread.run()

    def test_pre_exit_with_exception_in_function(self):
        """Pre_exit function error."""
        with self.assertRaises(thread_test.PreExitExecutionError):
            thread = self.thread_class(main=self.test_function_without_args,
                                       pre_exit=self.test_incorrect_function,
                                       pre_exit_args=self.params)
            thread.run()

    def test_multiple_loop_factor(self):
        """Multiple loop factor."""
        thread = self.thread_class(main=self.test_function_without_args,
                                   loop_factor=2)
        thread.start()
        thread.join()
        self.assertIsInstance(thread, thread_test.TestThread)

    def test_multiple_seconds_delay(self):
        """Longer delay test."""
        thread = self.thread_class(main=self.test_function_without_args,
                                   delay=2)
        thread.start()
        thread.loop_factor = -1
        thread.join()
        self.assertIsInstance(thread, thread_test.TestThread)


if __name__ == '__main__':
    unittest.main()
